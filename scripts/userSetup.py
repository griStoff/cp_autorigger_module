import os
import maya.cmds as mc


# scripts in the module
import cp_shelfBuilder_autorig as sB
import cp_proxyRigCreation as prox

class cp_autorig_shelf(sB._shelf):    
    def build(self):

        # skinning operations
        self.addButton(label="selectJoints", command= 'prox.checkFunction()')


def create_cp_autorigger():
    cp_autorig_shelf(name= 'cp_autorig_shelf')


mc.evalDeferred('createRiggingShelf()')

