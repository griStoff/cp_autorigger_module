import maya.cmds as mc
import maya.api.OpenMaya as om2
import sys
import os.path

from collections import OrderedDict as ordDict
import math





class jointCreation (object):
    def __init__(self, name= None, matrixInformation= None):
        '''
        
        asdfasdfasdfsadfsdf


        '''
        # set positions to 
        
        if name:
            self.jointName= name
        else:
            self.jointName= 'proxyJnt'

        # input nodeMatrix information
        if matrixInformation:
            if isinstance(matrixInformation, list):
                if len(matrixInformation)== 16:
                    self.matrixPos= matrixInformation
                    
                else:
                    print ('this is not a list containing 16 items, so a default matrix is used')
                    self.matrixPos= [1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]                   
                    
            else:
                if isinstance(matrixInformation, str):
                    
                    if mc.objExists(matrixInformation):
                        self.matrixPos= mc.xform(matrixInformation, matrix= True, query= True)
                    
                    else:
                        print('the object does not exist in the scene')
                        self.matrixPos= [1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]   
                else:
                    print('this is neither a string nor a list containing a matrix')
                    self.matrixPos= [1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]   
                
        else:
            print('there was no list input')
            self.matrixPos= [1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]   

    def createJoint():
        
        self.jointObject= mc.joint(n= self.jointName)
        mc.xform(self.jointObject, a= True, m= self.matrixPos)