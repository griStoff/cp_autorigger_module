import maya.cmds as mc
import maya.api.OpenMaya as om2




class controller(object):
    """creates a class called controller, and sets a classvariable

    Args:
        object (instanced class): instances the class object for usage

    Returns:
        [type]: returns nothing to my knowledge
    """
    
    suffix= 'ctrl'
    
    MATRIX_POSITION = (1.0, 0.0, 0.0, 0.0, 
                       0.0, 1.0, 0.0, 0.0, 
                       0.0, 0.0, 1.0, 0.0, 
                       0.0, 0.0, 0.0, 1.0
                       )
    
    def __init__(self, name= None, componentName= None, matrixInformation= None, parentNode= None, spacesDict= None):
        """initializes the class and processes inputs

        Args:
            name (string, optional): the name of the controller created by this class. Defaults to None.
            componentName (string, optional): the name of the prefix. Defaults to None.
            matrixInformation (list, optional): list containing a four by four matrix, so with len 16. Defaults to None.
            parentNode (unicode, optional): the string of the parent node if existent. Defaults to None.
            spacesDict (dict, optional): a dictionary with all spaces the control is ought to have. Defaults to None.
        """                     
        # input information #############################################################################################
        # input naming information
        if componentName:
            self.prefix = componentName
        else:
            self.prefix= 'prefix'
        
        if name:
            self.name= name
        else:
            self.name= 'defaultControl'
        
        self.nameCombined = '{prefix}_{name}_{suffix}'.format(prefix= self.prefix, name= self.name, suffix= self.suffix)


        # input nodeMatrix information
        if matrixInformation:
            if isinstance(matrixInformation, list):
                if len(matrixInformation)== 16:
                    self.matrixPos= matrixInformation
                    
                else:
                    print ('this is not a list containing 16 items, so a default matrix is used')
                    self.matrixPos= self.MATRIX_POSITION
                    
            else:
                if isinstance(matrixInformation, str):
                    
                    if mc.objExists(matrixInformation):
                        self.matrixPos= mc.xform(matrixInformation, matrix= True, query= True)
                    
                    else:
                        print('the object does not exist in the scene')
                        self.matrixPos= [1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]   
                else:
                    print('this is neither a string nor a list containing a matrix')
                    self.matrixPos= [1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]   
                
        else:
            print('there was no list input')
            self.matrixPos= [1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0]   


        if parentNode:
            if mc.objExists(parentNode):
                self.parentNode= parentNode

            else:
                self.parentNode= None
        else:
            self.parentNode= None


        # spaces information
        self.spacesDict= spacesDict
        

        # output information #############################################################################################
        # output control node
        self.control= None
        self.connectingAimMatrixAttribute= None
        self.controllerComponents= None



    
    # print info #########################################################################################################    
    def printInfo(self):        
        print (self.prefix, self.suffix, self.name)
        if self.parentNode:
            print('parent exists')
    
    # change names #######################################################################################################
    def changeSuffix(self, suffixChange):
        oldName= self.nameCombined
        
        self.suffix= suffixChange
        self.nameCombined = '{prefix}_{name}_{suffix}'.format(prefix= self.prefix, name= self.name, suffix= self.suffix)
        
        newName= self.nameCombined
        
        if mc.objExists(oldName):
            mc.rename(oldName, newName)
        
        
    def changePrefix(self, prefixChange):
        oldName= self.nameCombined
        
        self.prefix= prefixChange
        self.nameCombined = '{prefix}_{name}_{suffix}'.format(prefix= self.prefix, name= self.name, suffix= self.suffix)
        
        newName= self.nameCombined
        
        if mc.objExists(oldName):
            mc.rename(oldName, newName)
        
        
        
    def changeName(self, nameChange):
        oldName= self.nameCombined
        
        self.name= nameChange
        self.nameCombined = '{prefix}_{name}_{suffix}'.format(prefix= self.prefix, name= self.name, suffix= self.suffix)
        
        newName= self.nameCombined        
        
        if mc.objExists(oldName):
            mc.rename(oldName, newName)
        
        
    def changeNameCombined(self, nameCombinedChange= None, suffix= None, prefix= None, name= None):
        
        oldName= self.nameCombined
        
        if nameCombinedChange:
            self.nameCombined= nameCombinedChange
            
        else:
            if suffix:
                self.changeSuffix(suffix)
                
            if prefix:
                self.changePrefix(prefix)
                
            if name:
                self.changeName(name)
                
            self.nameCombined= '{prefix}_{name}_{suffix}'.format(prefix= self.prefix, name= self.name, suffix= self.suffix)        
        

        if mc.objExists(oldName):
            mc.rename(oldName, self.nameCombined)    
    

    # change shape color ##################################################################################################
    def changeObjectColor(self, input= None):
        if not input:
            shapeNodes= mc.listRelatives(self.control, shapes= True)
        else:
            shapeNodes= mc.listRelatives(input, shapes= True)
        if shapeNodes:
            for shape in shapeNodes:
                print('it fails here')
                nurbsCheck= mc.objectType(shape, isType= 'nurbsCurve')
                if nurbsCheck:
                    mc.setAttr('{node}.overrideEnabled'.format(node= shape), 1)
                    mc.setAttr('{node}.overrideColor'.format(node= shape), 22)
    

    # create controller ###################################################################################################
    def createController(self):
        
        self.controllerComponents= []

        # create the controlcurves right now it is a circle, but it will be a curve input          
        self.control= mc.joint(n= self.nameCombined)
        mc.addAttr(self.control, ln= 'cpController', attributeType= 'message')
        mc.addAttr(self.control, ln= 'aimUpvec', dataType= 'matrix')        
        
        # change the visual style of the controller
        mc.setAttr('{node}.radius'.format(node= self.control), channelBox= False, keyable= False, lock= True)
        mc.setAttr('{node}.drawStyle'.format(node= self.control), 2)
        

        # change the shape
        controlCurve= mc.circle()
        controlCurveShapes= mc.listRelatives(controlCurve, s= True)
        
        controlCurveShapesLen= len(controlCurveShapes)
        zFillLength= float(len(str(controlCurveShapesLen)))        


        if controlCurveShapes:
            for iteration, shape in enumerate(controlCurveShapes):
                mc.parent(shape, self.control, r= True, shape= True)
                self.controllerComponents.append(shape)
                

                # rename shapes from the curve
                shapeName= 'Shape'
                if controlCurveShapesLen > 1:
                    numberation= str(iteration+1).zfill(zFillLength)
                    shapeName= '{node}_{number}_{shape}'.format(node= self.control, number= numberation, shape= shapeName)
                
                shapeName= '{node}{shape}'.format(node= self.control, shape= shapeName)
                
                mc.rename(shape, shapeName)
                mc.delete(controlCurve)

        mc.delete(self.control, ch= True)

        # change the color of the controller
        self.changeObjectColor()

        # reset the scaling derived from the proxy objects
        mc.xform(self.control, matrix= self.matrixPos, worldSpace= True)
        mc.makeIdentity(self.control, scale= True, apply= True)
        self.matrixPos= mc.xform(self.control, matrix= True, worldSpace= True, query= True)


        self.controllerComponents.append(self.control)


        # check for a parent Node input and calculate the offset Parent Matrixm then parent the control under the parent         
        if self.parentNode:

            # get all parent information
            parentMatrix= mc.xform(self.parentNode, matrix= True, worldSpace= True, query= True)
            om2ParentMatrix= om2.MMatrix(parentMatrix)
            om2InversedParentMatrix= om2ParentMatrix.inverse()


            om2UsedMatrix= om2.MMatrix(self.matrixPos)


            om2CombinedMatrix= om2UsedMatrix
            om2ModifiedMatrix= om2CombinedMatrix* om2InversedParentMatrix 
            self.matrixPos= list(om2ModifiedMatrix)


            mc.parent(self.control, self.parentNode)
            

        else:
            print('no parent node was given, so basic matrix is used')
        
        
        mc.setAttr('{node}.offsetParentMatrix'.format(node= self.control), self.matrixPos, type= 'matrix')

        
        # create local z offset position
        self.connectingAimMatrixAttribute= '{node}.aimUpvec'.format(node= self.control)


        aimOffset= mc.createNode('composeMatrix', n= '{name}_aimOffset_cMtx'.format(name= self.name))
        mc.setAttr('{node}.inputTranslateZ'.format(node=aimOffset), 1)
        multMatrix= mc.createNode('multMatrix', n= '{name}_aimOffset_mMtx'.format(name= self.name))
        
        mc.connectAttr('{node}.outputMatrix'.format(node= aimOffset), '{node}.matrixIn[0]'.format(node= multMatrix))
        mc.connectAttr('{node}.worldMatrix[0]'.format(node= self.control),'{node}.matrixIn[1]'.format(node= multMatrix))
        

        mc.connectAttr('{node}.matrixSum'.format(node= multMatrix), self.connectingAimMatrixAttribute)

        
        self.controllerComponents.extend((aimOffset, multMatrix))
        
        # set the control to zero
        #mc.makeIdentity(self.control)
        axisList=['X','Y','Z']
        for axis in axisList:
            mc.setAttr('{node}.translate{axis}'.format(node= self.control, axis= axis), 0)
            mc.setAttr('{node}.jointOrient{axis}'.format(node= self.control, axis= axis), 0)
            mc.setAttr('{node}.rotate{axis}'.format(node= self.control, axis= axis), 0)
        
        mc.setAttr('{node}.segmentScaleCompensate'.format(node= self.control), False)
        
        
        # return the controlNode string
        return self.control

    def deleteController(self):

        if self.controllerComponents:
            mc.delete(self.controllerComponents)




class controlChain(object):
    '''

    lol

    '''

    def __init__(self, inputDict= None):
        self.inputDict= inputDict
        
        self.componentDictionary= dict()

    def createIkChain(self):
        print('not yet implemented')

    def createFkChain(self):
            if self.inputDict:
                if isinstance(self.inputDict, dict):
                    if len(self.inputDict) > 0:
                        # create a masterNode
                        
                        outputDict= {}
                        
                        inputList= list(self.inputDict.keys())
                            
                        for iteration, value in enumerate(inputList):
                            newClassName= '_'.join(value.split('_',3)[:3])

                            if iteration == 0:
                                outputDict[newClassName]= controller(name= newClassName, matrixInformation= self.inputDict[inputList[iteration]])
                                newControl= outputDict[newClassName].createController()
                        
                            else:
                                outputDict[newClassName]= controller(name= newClassName, parentNode= newControl, matrixInformation= self.inputDict[inputList[iteration]])
                                newControl= outputDict[newClassName].createController()
                            
                        return outputDict

    def createVariableFkRig(self):
        print('not yet implemented')


    def deleteControlChain(self):
        for key in self.componentDictionary:
            print (key)