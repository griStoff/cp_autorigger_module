
import maya.cmds as mc

import sys
import os.path

newPath= os.path.normpath("C:\\work\\cp_maya_scripts\\cp_maya_scripts\\autoRig\\generalFunctions\\")

if not newPath in sys.path:
    sys.path.append(newPath)
    
import cp_proxyRigCreation as proxy
reload (proxy)

controlCreation= proxy.proxyChain(chosenColor= 'red', limb= 'arm', inputObjects= ['one', 'two','three','four'])
controlCreation.createProxyChain()


controlCreation.getProxyInformation()

controlCreation.deleteProxyChain()

