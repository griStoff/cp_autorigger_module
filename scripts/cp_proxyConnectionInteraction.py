##################################
#
#     proxy connection interaction
#
#
##################################

import maya.cmds as mc
import maya.api.OpenMaya as om2


# connect the chain to a master -----------------------------------------------------------------------------------------------> migrate this into its own class or method
def connectMaster(inputMaster= None, inputSlave= None):
    
    connectionItems= mc.ls(sl= True)
    if connectionItems:
        if isinstance(connectionItems, list):
            itemAmount= len(connectionItems)
            if itemAmount == 2:
                listToOperate= connectionItems
            elif itemAmount > 2:
                listToOperate= connectionItems[0:2]
            else:
                print('please select at least two items for this operation')
                listToOperate= None
        
    if listToOperate:
        masterNode= listToOperate[0]
        masterAttr= 'slaveConnection'
        masterAttrName= '{node}.{attr}'.format(node= masterNode, attr= masterAttr)

        masterAttrCheck= mc.attributeQuery(masterAttr, node= masterNode, exists= True, message= True)

        print(masterNode+ ' is the masternode ' + str(masterAttrCheck))
        
        
        
        slaveNode= listToOperate[1]
        slaveAttr= 'masterConnection'
        slaveAttrName= '{node}.{attr}'.format(node= slaveNode, attr= slaveAttr)

        slaveAttrCheck= mc.attributeQuery(slaveAttr, node= slaveNode, exists= True, message= True)
        
        print(slaveNode+ ' is the slavenode ' + str(slaveAttrCheck))

        if masterAttrCheck and slaveAttrCheck:
            connectionCheck= mc.isConnected( masterAttrName, slaveAttrName )
            
            if not connectionCheck:
                mc.connectAttr(masterAttrName, slaveAttrName, f= True)
                connectionCheck= True

            else:
                print('the connection from {} to {} already existed, connection skipped'.format(masterNode.upper(), slaveNode.upper()))
                connectionCheck= None

        elif masterAttrCheck and not slaveAttrCheck:
            connectionCheck= None
            print('\n\nunluckily you can not connect "{}" to "{}", since you are not able to parent the components of a chain to another parent\n\n'.format(masterNode.upper(), slaveNode.upper()))

        elif slaveAttrCheck and not masterAttrCheck:
            connectionCheck= None
            print('this is not working either')
        
        else:
            connectionCheck= None
            print('fuuuuck')


        # sort out who can become a master: get component membership !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        masterOutgoingConnections= list()
        slaveOutgoingConnections= list()
        
        if mc.connectionInfo(masterNode+ '.componentMembership', isExactDestination= True):
            parentPlug= mc.connectionInfo(masterNode+ '.componentMembership', sourceFromDestination= True)
            
            # work back from the parent plug to get all connected children
            if parentPlug:
                parentName, parentAttr= parentPlug.split('.')
                isMessageCheck= mc.attributeQuery(parentAttr, n= parentName, message= True)

                if isMessageCheck:
                    mc.listConnections(parentPlug, destination= True)
                    
                    # sort the childNodes based on if they are a proxy Object or not
                    childNodes= [child for child in mc.listConnections(parentPlug, destination= True) if mc.attributeQuery('proxyJoint',n= child, exists= True, message= True)]
                    if childNodes:
                        for child in childNodes:
                            if mc.connectionInfo(child+ '.slaveConnection', destinationFromSource= True):
                                masterOutgoingConnections.append(child)


        # look for the connections in the slave node
        if mc.connectionInfo(slaveNode+ '.componentMaster', isExactSource= True):
            
            parentPlug= mc.connectionInfo(slaveNode+ '.componentMaster', getExactSource= True)
            #print (parentPlug)
            
            # work back from the parent plug to get all connected children
            if parentPlug:
                parentName, parentAttr= parentPlug.split('.')
                isMessageCheck= mc.attributeQuery(parentAttr, n= parentName, message= True)
                
                # sort the childNodes based on if they are a proxy Object or not
                if isMessageCheck:
                    childNodes= [child for child in mc.listConnections(parentPlug, destination= True) if mc.attributeQuery('proxyJoint',n= child, exists= True, message= True)]
                    if childNodes:
                        for child in childNodes:
                            if mc.connectionInfo(child+ '.slaveConnection', destinationFromSource= True):
                                slaveOutgoingConnections.append(child)       

        print(masterOutgoingConnections)

        if slaveOutgoingConnections:
            for slave in slaveOutgoingConnections:
                print(slave)