"""

_author = christof.puehringer


"""




import pymel.core as pm
import datetime
import logging

class RigBase(object):
    """
    _________________________________
    
        this class
    _________________________________
    """
    
    NETWORK_NAME = 'rigging_godnode'
    RIG_STING = 'RIG'
    SKELETON_STRING = 'skeleton'
    
    def __init__(self, rig='default'):
        
        self.rig_name = rig
        self.rigging_master = '{}_{}'.format(rig, self.NETWORK_NAME)

        if pm.objExists(self.rigging_master):
            logging.warning('rig godnode for "{rig_name}" already exists'.format(rig_name=self.rig_name))
            return None

        self.rigging_godnode = pm.createNode('network', n=self.rigging_master)
        
        #    rig information
        self.rigging_godnode.addAttr('rigVersion', dt='string')
        self.rigging_godnode.addAttr('mdlVersion', dt='string')
        self.rigging_godnode.addAttr('skinVersion', dt='string')
    
        #    rig connections
        self.rigging_godnode.addAttr('rig_modules', at='message', multi=True)
        self.rigging_godnode.addAttr('rig_base', at='message')
        self.rigging_godnode.addAttr('skeleton_export', at='message')
        
        #    build outliner structure
        self.build_outliner_base()
        
            
    def build_outliner_base(self):
        
        #    structure base
        rigging_structure_base = pm.createNode('transform', n='{}_{}'.format(self.rig_name, self.RIG_STING))
        rigging_structure_base.addAttr('rig_membership', at='message')
        self.rigging_godnode.rig_base.connect(rigging_structure_base.rig_membership)
        
        #    export skeleton
        rigging_export_skeleton = pm.createNode('transform', n='export{}_GRP'.format(self.SKELETON_STRING.title()))
        rigging_export_skeleton.addAttr('rig_membership', at='message')
        self.rigging_godnode.skeleton_export.connect(rigging_export_skeleton.rig_membership)
        
        pm.parent(rigging_export_skeleton, rigging_structure_base)        
    

RigBase()