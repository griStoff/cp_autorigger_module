"""_summary_
    
__author__ = christof.puehringer
    
"""


#   external modules
import os
import time
import logging
import json

#   internal modules
import maya.cmds as mc
import pymel.core as pm
import maya.api.OpenMaya as om
import maya.mel as mel

#   custom modules
import version_control

#   establish logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)



#   GLOBAL VARIABLES #################################################

#   test variables
TEST_MESH = 'mesh'
TEST_DIVISIONS = 20
TEST_AMOUNT = 20

#   static variables
EXPORT_TYPE = '.json'



#   START OF OPERATIONS ##############################################

#   debug operations
def create_sphere_skinned():
    
    #   open new file
    pm.newFile(force=True, type='mayaAscii')
    logger.debug('new file opened')

    #   create testobject
    pm.polySphere(n=TEST_MESH, ch=False, sa= TEST_DIVISIONS, sh= TEST_DIVISIONS)
    logger.debug('created a polysphere called "{}"'.format(TEST_MESH))

    #   create joints on testobject
    joints = list()
    for i in range(0, TEST_AMOUNT):
        mc.select(clear=True)
        jt = mc.joint(n='{}_{}_jnt'.format(TEST_MESH, str(i).zfill(len(str(TEST_AMOUNT)))))
        joints.append(jt)

    logger.debug('created {} joints on "{}"'.format(str(TEST_AMOUNT), TEST_MESH))

    #   skin joints

    mc.skinCluster(joints, TEST_MESH, n = '{}_skC'.format(TEST_MESH), tsb= True)
    logger.debug('applied skincluster on "{}"'.format(TEST_MESH))
    mc.select(clear=True)

    return True

def dev_save():
    pass

def dev_load():
    pass

def run_test_all():
    pass


#   BASECLASS ##############################################
class SkinClusterIO(object):
    def __init__(self, dirpath=None):
        
        self.filepath = None
        self.dirpath = None
        
        #   skin information
        self.deformer_data = None
        
        #   mesh information
        self.mesh_object = None
        self.mesh_name = None

        #   scene information
        self.scene_name = None
        self.scene_directory = None

        #   file information
        self.file_name = None

    def get_data(self):
        pass
    
    def set_data(self):
        pass
    
    def save_data(self):
        pass
    
    def load_data(self):
        pass

    def export_data(self):
        
        data_get_successful = self.get_data()
        
        if not data_get_successful:
            return False

        self.save_data()
        
        return True

    def import_data(self):

        data_get_successful = self.get_data()
        
        if not data_get_successful:
            return False

        self.set_data()
        
        return True


# JSON ##############################################
class SkinClusterJsonIO(SkinClusterIO):
    

    def __init__(self, dirpath= None):
        
        super().__init__(dirpath=dirpath)
        
        #   assign type to deformer data
        self.deformer_data = dict()


    def get_data(self, node=None):
        
        #   check for mesh node to query
        if node == None:
            node = pm.ls(sl=True)
            logger.debug('no node was given, defaulting to selected item')

        if not node:
            logger.error('no item was selected')
            return False

        if isinstance(node, list):
            node = node[0]

        node_name = node.name()

        #   check for a skincluster on mesh
        skincluster = mel.eval('findRelatedSkinCluster {}'.format(node_name))
        if not skincluster:
            logger.error('selected item: "{}" has no valid skincluster'.format(node_name))
            return False

        #   start timer
        operation_start_time = time.time()

        #   vertex weight get
        shape = node.getShape()
        shape_name = shape.name()

        verts = ['{}.vtx[{}]'.format(shape_name, x) for x in mc.getAttr('{}.vrts'.format(shape_name), multiIndices=True)]
        for v in verts:
            influences = mc.skinPercent(skincluster, v, transform=None, query=True)
            weights = mc.skinPercent(skincluster, v, value=True, query=True)
            vertex_data = tuple(zip(influences, weights))

            self.deformer_data[v] = vertex_data

        #   stop timer and return info to user
        operation_end_time = time.time()
        operation_elapsed_time = operation_end_time - operation_start_time
        logger.info('method "def get_data()" finished in {}'.format(operation_elapsed_time))

        return True


    def set_data(self):
        
        #   check if the skinning file has information in it
        if 'bom':
            logger.error('no data has been found in the file')
            return False

        return True


    def save_data(self):
        
        #   check if the deformation data has been extracted
        if not self.deformer_data:
            logger.error('there is no data to save')
            return False
        
        #   start timer
        operation_start_time = time.time()

        #   write information to disk
        file_json = open('C:\work\projects\maya\python\data.json', 'w')
        json.dump(self.deformer_data, file_json)
        file_json.close()
        
        #   stop timer and return info to user
        operation_end_time = time.time()
        operation_elapsed_time = operation_end_time - operation_start_time
        logger.info('method "def save_data()" finished in {}'.format(operation_elapsed_time))

        return True


    def load_data(self):

        #   check if the deformation data has been extracted
        if not self.deformer_data:
            logger.error('there is no data to save')
            return False
        
        #   start timer
        operation_start_time = time.time()

        #   write information to disk
        file_json = open('C:\work\projects\maya\python\data.json', 'r')
        json.dump(self.deformer_data, file_json)
        file_json.close()
        
        #   stop timer and return info to user
        operation_end_time = time.time()
        operation_elapsed_time = operation_end_time - operation_start_time
        logger.info('method "def save_data()" finished in {}'.format(operation_elapsed_time))

        return True


# NUMPY ##############################################
class SkinClusterNumpyIO(SkinClusterIO):
    
    def __init__(self, dirpath=None):
        super().__init__(dirpath=dirpath)
    
    def get_data(self):
        pass
    
    def set_data(self):
        pass
    
    def save_data(self):
        pass
    
    def load_data(self):
        pass
    
    def compress_data(self):
        pass

SkinClusterJsonIO().export_data()



'''

def getCurrentWeights(skinCls):
    
    """Get the skincluster weights

    Arguments:
        skinCls (PyNode): The skincluster node
        dagPath (MDagPath): The skincluster dagpath
        components (MObject): The skincluster components

    Returns:
        MDoubleArray: The skincluster weights

    """
    
    weights = om.MDoubleArray()
    
    loc = pm.PyNode('mesh')
    loc_shape = loc.getShape()
    dagpath = loc.__apimdagpath__()
    
    util = om.MScriptUtil()
    util.createFromInt(0)
    pUInt = util.asUintPtr()
    
    components = loc_shape.__apimfn__().getVertices()

    skinCls.__apimfn__().getWeights(dagPath, components, weights, pUInt)
    
    return weights


clusterNode= pm.PyNode('mesh_skC')
getCurrentWeights(clusterNode)

'''