"""
Gui module for the setting of attributes.
"""

# Import future libraries
# Import future modules
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Import built-in modules
import logging
import re
import sys

from shiboken2 import wrapInstance

from builtins import object
from builtins import str

from PySide2 import QtCore
from PySide2 import QtGui
from PySide2 import QtWidgets

from itertools import chain

from typing import Optional, Any, List, Set
from future import standard_library

# Import third-party modules
# Import Maya specific modules
import maya.cmds as cmds
import maya.OpenMayaUI as omui
import maya.api.OpenMaya as om2


if sys.version_info.major == 3:
    from importlib import reload
elif sys.version_info.major == 2:
    pass

##########################################################
# GLOBALS
##########################################################

__author__ = "Christof Puehringer"

standard_library.install_aliases()
_LOGGER = logging.getLogger(__name__ + ".py")
_LOGGER.setLevel(logging.INFO)

WINDOW_TITLE = "AttributeSetterUi"

PATTERN_WHITESPACES = re.compile(r"\w+")

##########################################################
# CLASSES
##########################################################

from functools import wraps
from time import time



def timing(f):
    @wraps(f)
    def wrap(*args, **kw):
        ts = time()
        result = f(*args, **kw)
        te = time()
        print('func:%r args:[%r, %r] took: %2.4f sec' % \
              (f.__name__, args, kw, te - ts))
        return result

    return wrap


def maya_main_window():
    """
    Return the Maya main window widget as a Python object
    """
    main_window_ptr = omui.MQtUtil.mainWindow()

    if sys.version_info.major >= 3:
        return wrapInstance(int(main_window_ptr), QtWidgets.QWidget)
    else:
        return wrapInstance(long(main_window_ptr), QtWidgets.QWidget)


class AttributeSetterUi(QtWidgets.QDialog):

    """

    Ui Responsible to set Attributes not by selection but by type or name of the objects.

    """

    WINDOW_TITLE_ = WINDOW_TITLE

    def __init__(self, parent=maya_main_window()):
        super(AttributeSetterUi, self).__init__(parent)

        self.setWindowTitle(self.WINDOW_TITLE_)
        self.setObjectName(self.WINDOW_TITLE_)

        if cmds.about(ntOS=True):
            self.setWindowFlags(self.windowFlags() ^ QtCore.Qt.WindowContextHelpButtonHint)

        elif cmds.about(macOS=True):
            self.setWindowFlags(QtCore.Qt.Tool)

        self.setMinimumWidth(550)
        self.setMinimumHeight(320)

        self.create_widgets()
        self.create_layout()
        self.create_connections()

    def create_widgets(self):

        # left side
        self.left_widget = QtWidgets.QWidget()
        self.items_descritption = QtWidgets.QLabel("Item Selection:")

        self.nodes_allowed = QtWidgets.QCheckBox("Allow Types")
        self.names_allowed = QtWidgets.QCheckBox("Allow Names")

        self.node_search_field = QtWidgets.QLineEdit()
        self.node_search_list = QtWidgets.QListWidget()
        self.node_search_list.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

        self.select_highlighted_btn = QtWidgets.QPushButton("Ui -> Scene")
        self.highlight_selected_btn = QtWidgets.QPushButton("Scene -> Ui")

        # right side
        self.right_widget = QtWidgets.QWidget()
        self.attributes_descritption = QtWidgets.QLabel("Attrribute Selection:")

        self.attr_search_field = QtWidgets.QLineEdit()
        self.attr_search_list = QtWidgets.QListWidget()
        self.attr_search_list.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

        self.attribute_value_input = QtWidgets.QLineEdit()
        self.set_attrs_btn = QtWidgets.QPushButton("Set Attributes")

    def create_layout(self):
        center_layout = QtWidgets.QHBoxLayout()

        # allowance layout
        options_layout = QtWidgets.QHBoxLayout()
        options_layout.addWidget(self.nodes_allowed)
        options_layout.addWidget(self.names_allowed)

        # conversion layout
        conversion_layout = QtWidgets.QHBoxLayout()
        conversion_layout.addWidget(self.select_highlighted_btn)
        conversion_layout.addWidget(self.highlight_selected_btn)
        center_layout.addStretch()

        # left layout
        left_layout = QtWidgets.QVBoxLayout()
        left_layout.addStretch()

        left_layout.addWidget(self.items_descritption)
        left_layout.addLayout(options_layout)

        left_layout.addWidget(self.node_search_field)
        left_layout.addWidget(self.node_search_list)

        left_layout.addLayout(conversion_layout)

        self.left_widget.setLayout(left_layout)

        # right layout
        right_layout = QtWidgets.QVBoxLayout()
        right_layout.addStretch()

        right_layout.addWidget(self.attributes_descritption)

        right_layout.addWidget(self.attr_search_field)
        right_layout.addWidget(self.attr_search_list)

        right_layout.addWidget(self.attribute_value_input)
        right_layout.addWidget(self.set_attrs_btn)

        self.right_widget.setLayout(right_layout)

        main_layout = QtWidgets.QSplitter(self)

        main_layout.addWidget(self.left_widget)
        main_layout.addWidget(self.right_widget)

        center_layout.addWidget(main_layout)

    def create_connections(self):
        self.node_search_field.textChanged.connect(self.set_selected_types)

        self.nodes_allowed.toggled.connect(self.set_selected_types)
        self.names_allowed.toggled.connect(self.set_selected_types)

        self.select_highlighted_btn.pressed.connect(self.select_highlighted)
        self.highlight_selected_btn.pressed.connect(self.highlight_selected)

        self.attr_search_field.textChanged.connect(self.set_selected_attrs)

        self.node_search_list.itemSelectionChanged.connect(self.set_selected_attrs)

        self.set_attrs_btn.pressed.connect(self.set_attribute_values_to_ui)

    def set_selected_types(self):

        old_selection = [str(sel_item_.text()) for sel_item_ in self.node_search_list.selectedItems()]

        self.node_search_list.clear()

        search_string = self.node_search_field.text()
        types_allowed_ = self.nodes_allowed.checkState()
        names_allowed_ = self.names_allowed.checkState()

        all_availables = get_selected_types(search_string,
                                            types_allowed=types_allowed_,
                                            names_allowed=names_allowed_
                                            )

        if not all_availables:
            return

        self.node_search_list.addItems(all_availables)

        if not old_selection:
            return True

        for old_sel in old_selection:
            matching_items = self.node_search_list.findItems(old_sel, QtCore.Qt.MatchExactly)
            for item in matching_items:
                item.setSelected(True)

        return True

    def set_selected_attrs(self):
        """Sets the Attribute list to the Available Attributes in the Items selection.

        Args:
            search_query (str): The string of which to search for the attributes.
            items_for_iteration (list | None, optional): The items . Defaults to None.

        Returns:
            list: _description_
        """

        self.attr_search_list.clear()

        search_string = self.attr_search_field.text()
        selected_items = self.node_search_list.selectedItems()

        if not selected_items:
            return

        selected_items = [str(selected_item_.text())
                          for selected_item_
                          in selected_items
                          ]

        available_attributes_ = get_selected_attrs(search_string,
                                                   items_for_iteration=selected_items
                                                   )

        self.attr_search_list.addItems(available_attributes_)

    def set_attribute_values_to_ui(self):

        selected_items = [str(selected_item_.text())
                          for selected_item_
                          in self.node_search_list.selectedItems()
                          ]

        selected_attr = [str(selected_attr_.text())
                         for selected_attr_
                         in self.attr_search_list.selectedItems()
                         ]

        input_item = self.attribute_value_input.text()

        set_attribute_values(input_item,
                             nodes=selected_items,
                             attributes=selected_attr
                             )

    def select_highlighted(self):

        cmds.select([str(selected_item_.text())
                     for selected_item_
                     in self.node_search_list.selectedItems()
                     if cmds.objExists(str(selected_item_.text()))]
                    )

    def highlight_selected(self):
        types_allowed_ = self.nodes_allowed.checkState()
        names_allowed_ = self.names_allowed.checkState()

        names_ = set()
        types_ = set()

        if names_allowed_:
            names_ = {str(sel_item) for sel_item in cmds.ls(sl=True)}

        if types_allowed_:
            types_ = {str(cmds.objectType(sel_item)) for sel_item in cmds.ls(sl=True)}

        search_items = sorted(
                list(names_ | types_))

        self.node_search_field.setText(" ".join(search_items))


def list_of_lists_to_unique_set(input_items: list) -> set:
    return set(
            chain.from_iterable(
                    input_items
            )
    )


def get_selected_types(search_query: str, types_allowed: bool = True, names_allowed: bool = True,
                       defaults_allowed: bool = True
                       ) -> list:

    if not any((types_allowed, names_allowed)):
        return

    available_nodes = set()
    available_names = set()

    # TO DO: maybe switch the search query and the types allowed

    search_query_items = re.findall(PATTERN_WHITESPACES, search_query)

    if types_allowed:
        
        all_nodes = get_all_available_nodes()

        available_nodes = all_nodes
        
        if search_query:
    
            matching_node_names = find_full_words_with_items(search_query_items, all_nodes)
            available_nodes = list_of_lists_to_unique_set(
                    get_all_nodes_of_type(available_node_type) for available_node_type in matching_node_names)

    if names_allowed:
        all_names = get_all_available_names()

        available_names = all_names
        
        if search_query:
            all_names = get_all_available_names()
    
            available_names = find_full_words_with_items(search_query_items, all_names)

    return sorted(available_nodes | available_names)


@timing
def get_selected_attrs(search_query: str, items_for_iteration: Optional[list] = None) -> list:

    if not items_for_iteration:
        return

    available_attrs = list_of_lists_to_unique_set(
            [
                cmds.listAttr(selected_item, se=True)
                for selected_item in items_for_iteration
            ]
    )

    if not search_query:
        return sorted(available_attrs)

    search_strings = re.findall(PATTERN_WHITESPACES, search_query)

    return sorted([
        x
        for x in available_attrs
        for search_string in search_strings
        if search_string in x
    ])


@timing
def set_attribute_values(value_to_set: Any, nodes: Optional[list] = None, attributes: Optional[list] = None) -> True:

    if not any(nodes):
        raise ValueError("no items selected in the scene object list")

    if not any(attributes):
        raise ValueError("no attributes selected in the attributes list")

    nodes_and_attrs = [
        f"{node_}.{attr_}"
        for node_ in nodes
        for attr_ in attributes
        if has_attribute(node_, attr_)
    ]

    if not nodes_and_attrs:
        raise ValueError("no attributes were found on meshes")

    values_to_set = [val_.strip() for val_ in value_to_set.split(",")]

    is_numeric = True

    try:
        value_to_set = [float(value_to_set) for value_to_set in values_to_set]

    except ValueError:
        is_numeric = False

    for attr__ in nodes_and_attrs:
        is_settable = cmds.getAttr(attr__, settable=True)

        if not is_settable:
            continue

        attr_type = cmds.getAttr(attr__, type=True)
        print(attr_type)
        if attr_type in ("doubleLinear", "float", "integer") and is_numeric:
            cmds.setAttr(attr__, value_to_set[0])
            continue

        elif attr_type in ("string", ) and not is_numeric:
            print(attr_type)

            cmds.setAttr(attr__, value_to_set, type=attr_type)

        else:
            print(attr_type)

            cmds.setAttr(attr__, *value_to_set, type=attr_type)

    return True

def has_attribute(node_name, attribute_name):
    sel = om2.MGlobal.getSelectionListByName(node_name)
    mobj= sel.getDependNode(0)
    
    dependency_nd = om2.MFnDependencyNode(mobj)
    
    return dependency_nd.hasAttribute(attribute_name)
    
   

def find_full_words_with_items(list_items, input_strings):
    # Escape and join list items into a single regex pattern
    escaped_items = '|'.join(map(re.escape, list_items))

    pattern = re.compile(rf'\b\w*(?:{escaped_items})\w*\b', re.IGNORECASE)

    # Use a generator to yield matches from the input strings
    return set(match.group() for string in input_strings for match in pattern.finditer(string))


def get_all_available_nodes() -> Set[str]:
    """Retrieve all available node types from the environment."""
    return set(get_all_available_nodes_implementation())


def get_all_available_nodes_implementation():
    dg_iterator = om2.MItDependencyNodes(om2.MFn.kInvalid)

    dg_node_fn = om2.MFnDependencyNode()

    while not dg_iterator.isDone():

        dg_object = dg_iterator.thisNode()
        dg_node_fn.setObject(dg_object)

        yield dg_node_fn.typeName
        dg_iterator.next()

def get_all_nodes_of_type(node_type):
    node_type_ = node_type

    dg_iterator = om2.MItDependencyNodes(om2.MFn.kInvalid)

    dg_node_fn = om2.MFnDependencyNode()

    while not dg_iterator.isDone():

        dg_object = dg_iterator.thisNode()
        dg_node_fn.setObject(dg_object)

        if dg_node_fn.typeName == node_type_:
            yield dg_node_fn.name()

        dg_iterator.next()


def get_all_available_names() -> Set[str]:
    """Retrieve all available node names from the environment."""
    return set(get_all_available_names_implementation())


def get_all_available_names_implementation():
    dg_iterator = om2.MItDependencyNodes(om2.MFn.kInvalid)

    dg_node_fn = om2.MFnDependencyNode()

    while not dg_iterator.isDone():

        dg_object = dg_iterator.thisNode()
        dg_node_fn.setObject(dg_object)

        yield dg_node_fn.name()
        dg_iterator.next()


def main():
    maya_main_window_ = maya_main_window()
    attribute_setter = maya_main_window_.findChild(QtWidgets.QDialog, WINDOW_TITLE)

    # if there is no attribute setter ui
    if not attribute_setter:
        attribute_setter = AttributeSetterUi()
        attribute_setter.show()
        return True

    # if there is
    try:
        attribute_setter.close()  # pylint: disable=E0601
        attribute_setter.deleteLater()
    except:
        pass

    attribute_setter = AttributeSetterUi()
    attribute_setter.show()
    return True


if __name__ == "__main__":
    main()