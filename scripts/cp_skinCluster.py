import pymel.core as pm

def get_joint_influences():
    selects= pm.selected()
    returns= list()

    if selects:
        for s in selects:
            try:
                returns.extend(pm.skinCluster(s, q= True, influence= True))

            except:
                pass

        return returns

    else:
        return None

def sel_joint_influences():
    pm.select(get_joint_influences())