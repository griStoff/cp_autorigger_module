import maya.cmds as mc


class transferAttrProxy(object):
    
    def __init__(self, startAttr= None, goalAttr= None):

        self.startAttr= startAttr

        self.goalAttr= goalAttr

    def transferProxy(self):
        
        if self.startAttr and self.goalAttr:

            mc.addAttr(ln= self.startAttr, proxy= self.goalAttr)


proxyTransfer= transferAttrProxy(startAttr= 'order', goalAttr= 'arm_two_proxy.order')

proxyTransfer.transferProxy()