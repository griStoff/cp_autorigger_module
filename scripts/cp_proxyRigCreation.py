import maya.cmds as mc
import maya.api.OpenMaya as om2
import sys
import os.path

from collections import OrderedDict as ordDict
import math





# import custom tools


newPath= os.path.normpath("C:\\work\\cp_maya_scripts\\cp_maya_scripts\\standardOps\\curveTools")

if not newPath in sys.path:
    sys.path.append(newPath)
    
import cp_curveTools
reload (cp_curveTools)






# creates two vectors from input one to input two and three, and uses them to calculate the cross product of those two
def calculateCrossproduct(inputOne, inputTwo, inputThree):
    
    """calculates the crossproduct out of three inputs where the line is drawn from one to two and one to three

    Args:
        inputOne (string): the string of the first node which location is looked up
        inputTwo (string): the string of the second node which location is looked up
        inputThree (string): the string of the third node which location is looked

    Returns:
        list: returns a list containing the normalized crossproduct between the vectors drawn
    """     
    
    
    
    #worldVectorBase= adfs
    slaveVector= mc.xform(inputOne, query= True, translation= True, worldSpace= True)
    
    mayaWorldVectorOne= mc.xform(inputTwo, query= True, translation= True, worldSpace= True)
    mayaWorldVectorTwo= mc.xform(inputThree, query= True, translation= True, worldSpace= True)
    
    # convert it into vectors ready for crossproduct
    #omWorldVectorBase= asdf

    omSlaveVector= om2.MVector(slaveVector)

    omVectorOne= om2.MVector(mayaWorldVectorOne)
    omVectorTwo= om2.MVector(mayaWorldVectorTwo)

    omPlacedVectorOne= omSlaveVector - omVectorOne
    omPlacedVectorTwo= omSlaveVector - omVectorTwo

    # calculate crossproduct
    omUpVector= omPlacedVectorOne ^ omPlacedVectorTwo
    omUpVectorNormalized= omUpVector.normal()
    omVectorList= [omUpVectorNormalized[0], omUpVectorNormalized[1], omUpVectorNormalized[2]]
    
    # change upVector Result to the world
    omUpVectorNormalized
    
    return omVectorList


def checkFunction():
    leftArm= proxyChain(chosenColor= 'red', limb= 'Lf_arm', inputObjects= ['one', 'two','three','four'])
    leftArm.createProxyChain()

    rightArm= proxyChain(chosenColor= 'red', limb= 'Rt_arm', inputObjects= ['one', 'two','three','four'])
    rightArm.createProxyChain()

def main():
    pass


# proxyObject class creation
class proxyObject(object):

    def __init__(self, componentName= None, inputName= 'defaultProxyObject', chosenColorValue= (1, 1, 0.2), iterationInput= 0, listLength= 1):

        print ('proxy Object for {component}'.format(component= inputName))

        
        # proxyObject information
        self.iteration= iterationInput
        self.listLength= listLength
        
        self.componentName= componentName
        self.middleName= inputName
        self.suffix= 'proxy'
        self.composedName= '{prefix}_{node}_{suffix}'.format(prefix= self.componentName, node= self.middleName, suffix= self.suffix)
                
        # prepare parenting information connections
        self.slaveAttr= 'slaveConnection'
        self.slave= '{node}.{attr}'.format(node= self.composedName, attr= self.slaveAttr)
        
        
        # proxyObject information
        self.proxyObject= None
        self.sphereRadius= round(1-((1/float(self.listLength)*float(self.iteration))), 3)

        print (str(self.sphereRadius) + ' is part of ' + str(self.composedName))
        

        self.proxyComponentList= []
       
        
        # proxyObject shading information
        self.chosenColorValue= chosenColorValue
        self.shaderName= '{component}_{node}_msMat'.format(component= self.componentName, node= self.middleName)
        self.proxyObjectShader= None

        
        # proxyObjectAuxNodes information
        self.decomposeMatrixName= '{component}_{node}_dMtx'.format(component= self.componentName, node= self.middleName)
        self.decomposeMatrix= None
        self.proxyWorldMtx= None


    def createProxyObject(self):
            
        # create proxy geometry
        self.proxyObject= mc.sphere(n= '{node}'.format(node= self.composedName), radius= 1)[0]
        
        mc.xform(self.proxyObject, scale=[self.sphereRadius, self.sphereRadius, self.sphereRadius])
        mc.delete(self.proxyObject, constructionHistory= True)

        self.proxyComponentList.append(self.proxyObject)
        
        
        # create slaveConnection attr
        mc.addAttr(self.composedName, ln= self.slaveAttr, attributeType= 'message')


        # set attributes to a usable channelboxformat
        endingAxis= ['x', 'y','z']
        for axis in endingAxis:
            mc.setAttr('{node}.r{axis}'.format(node= self.proxyObject, axis= axis), lock= False, keyable= False, channelBox= False)
            mc.setAttr('{node}.s{axis}'.format(node= self.proxyObject, axis= axis), lock= False, keyable= False, channelBox= False)


        # apply material to object
        self.appliedMaterial= self.applyMaterial()


        # create decompose matrix
        self.decomposeMatrix= mc.createNode('decomposeMatrix', n= self.decomposeMatrixName)
        self.proxyComponentList.append(self.decomposeMatrix)


        # connect woldMatrix to decomposition
        mc.connectAttr('{node}.worldMatrix[0]'.format(node= self.proxyObject), '{node}.inputMatrix'.format(node= self.decomposeMatrix))
    

        # write information for further processing
        self.proxyWorldMtx= '{node}.outputTranslate'.format(node= self.decomposeMatrix)
        

        return self.proxyObject
    
    
    def renameProxyObject(self):
        
        # check if list exists and rename Objects in the list
        if self.proxyComponentList:
            print (self.proxyComponentList)
        
        else:
            print ('proxy Object has not yet been created')


    def applyMaterial(self):
    
        if self.proxyObject:
            self.proxyObject= self.proxyObject
            
            if mc.objExists(self.proxyObject):
                if mc.objectType(self.proxyObject, isType='transform'):
                    
                    if not mc.objExists(self.shaderName):
                        self.proxyObjectShader= mc.shadingNode('surfaceShader', name= self.shaderName, asShader=True)
                        mc.addAttr(self.proxyObjectShader, ln='proxyShader', attributeType= 'message')

                        shdSG = mc.sets(name='{node}SG'.format(node= self.proxyObjectShader), empty=True, renderable=True, noSurfaceShader=True)
                        mc.addAttr(shdSG, ln='proxyShader', attributeType= 'message')

                        mc.connectAttr('{node}.outColor'.format(node= self.proxyObjectShader), '{node}.surfaceShader'.format(node= shdSG))
                    
                    else:
                        shdSG= '{node}SG'.format(node= self.shaderName)
            
                    mc.sets(self.proxyObject, e= True, forceElement= shdSG)
                    mc.setAttr('{matNode}.outColor'.format(matNode= self.proxyObjectShader), self.chosenColorValue[0], self.chosenColorValue[1], self.chosenColorValue[2], type="double3")
                    
                    self.proxyComponentList.append(self.proxyObjectShader)
                    self.proxyComponentList.append(shdSG)

                    return self.proxyObjectShader
                
                else:
                    print('input a transform please')
                    return None
        
        else:
            print('no inputNode was specified')
            return None
    

    def changeMaterialColor(self):
        mc.setAttr('{matNode}.outColor'.format(matNode= self.proxyObjectShader), self.chosenColorValue[0], self.chosenColorValue[1], self.chosenColorValue[2], type="double3")



# constructor class creation
class constructor(object):
    def __init__(self, name= None, chosenColorValue= None):
        if name:
            self.constructorName= name
        else:
            self.constructorName= 'proxyConstructor'

        print(mc.objExists(self.constructorName))
        self.constructor= None

        if chosenColorValue:
            self.chosenColorValue= chosenColorValue
        else:
            self.chosenColorValue=[1,1,0]


        # prepare parenting information connections
        self.masterAttr= 'masterConnection'
        self.slaveAttr= 'slaveConnection'

        self.masterAttrName= '{node}.{attr}'.format(node= self.constructorName, attr= self.masterAttr)
        self.slaveAttrName= '{node}.{attr}'.format(node= self.constructorName, attr= self.slaveAttr)


        # empty list for all created nodes for easy deletion
        self.allComponents= []

    def createConstructor(self):
        # creationHelperNode creation
        self.constructor= mc.group(n= '{0}'.format(self.constructorName), empty= True)
        
        # parent curveshape into group
        curveShapeTransform= mc.curve(n= 'cp_curveProxy', p= cp_curveTools.cp_loadJsonCurve("cube_crv", "C:\\work\\cp_maya_scripts\\setup\\curves")[1], degree= 1)

        curveShapes= mc.listRelatives(curveShapeTransform, s= True)
        
        if curveShapes:
            shapeAmount= len(curveShapes)
            zfillAmount= len(str(shapeAmount))+1
            for iter, i in enumerate(curveShapes):
                if mc.objectType(i, isType='nurbsCurve'):
                    mc.parent(i, self.constructor, r= True, shape= True)
                    shapeNodeName= '{}_{}Shape'.format(self.constructorName, str(iter).zfill(zfillAmount))
                    mc.rename(i, shapeNodeName)
                    

                    mc.setAttr('{node}.overrideEnabled'.format(node= shapeNodeName), True)
                    mc.setAttr('{node}.overrideRGBColors'.format(node= shapeNodeName), True)
                    
                    channelList= ['R', 'G', 'B']

                    for iteration, channel in enumerate(channelList):
                        mc.setAttr('{node}.overrideColor{channel}'.format(node= shapeNodeName, channel= channel), self.chosenColorValue[iteration])

                    self.allComponents.append(shapeNodeName)
                    mc.delete(curveShapeTransform)
        
        mc.delete(self.constructor, ch= True)
        
        mc.addAttr(self.constructor, ln='componentMaster', attributeType= 'message')
        mc.addAttr(self.constructor, ln='jointConnections', attributeType= 'message')
        mc.addAttr(self.constructor, ln= 'componentType', attributeType= 'message')


        mc.addAttr(self.constructor, ln= self.masterAttr, attributeType= 'message')
        mc.addAttr(self.constructor, ln= self.slaveAttr, attributeType= 'message')

# proxyChain class creation
class proxyChain(object):

    def __init__(self, limb= None, chosenColor= 'YELLOW', inputPositionsList= None, inputObjects= None, masterAttr= None, slaveAttr= None):
        
        print ('\n\n##############################\nproxyChain Initialized for {component}\n##############################\n\n'.format(component= limb.upper()))
        
        # single object information
        self.chosenColor= chosenColor.upper()


        # colorinformation
        self.availableColors= {'RED': (0.8, 0.15, 0), 'YELLOW': (1, 1, 0.2), 'BLUE': (0.1, 0.55, 0.85), 'GREEN': (0.2, 0.9, 0.2)}
        self.chosenColorValue= self.availableColors[self.chosenColor]


        # inputList and inputAmount get
        if isinstance(inputObjects, list):
            self.inputObjects= inputObjects
        
        elif isinstance(inputObjects, int):
            self.inputObjects= list(range(1, inputObjects+1))
        
        elif isinstance(inputObjects, float):
            self.inputObjects= list(range(1, math.floor(inputObjects)+1))
        
        else:
            self.inputObjects= None

        # inputObjects amount and scaling 
        self.inputObjectsAmount= None
        self.inputObjectScaling= None
        
        if self.inputObjects:
            # length of the working list
            self.inputObjectsAmount= len(self.inputObjects)
            
            # scaling derived from the workinglist
            if self.inputObjectsAmount > 1:
                self.inputObjectScaling= (1.0/float(self.inputObjectsAmount))  

            elif self.inputObjectsAmount == 1:
                self.inputObjectScaling= 1



        # inputPositions get
        self.inputPositions= inputPositionsList

        self.allList= []
        self.nodesWithoutMasterList= []
        self.signifierList= []
        

        # naming information #####################################################################################################
        if limb:
            self.limb= str(limb)
        else:
            self.limb= 'proxyLimb'


        # constructorinformation #################################################################################################
        self.constructor= None
        self.constructorName= '{node}_constructor_grp'.format(node= self.limb)

        self.constructorMatrix= None
    
        self.masterAttrName= self.constructorName + '.masterConnection'
        self.slaveAttrName= self.constructorName + '.slaveConnection'
        
        # curve information


        # filling information
        self.outputDict= ordDict({})
        self.zippedDict= ordDict({})


    def createProxyChain(self):

        if self.inputObjects:
            self.nameList= self.inputObjects
            
            self.constructorObject= constructor(name= self.constructorName, chosenColorValue= self.chosenColorValue)
            self.constructorObject.createConstructor()

            self.constructor= self.constructorObject.constructor
            self.allList.append(self.constructorObject.constructor)
            self.allList.extend(self.constructorObject.allComponents)


            # create the connectionCurve #######################################################
            curvePointList = [(0,0,0)]* self.inputObjectsAmount
            connectionCurve= mc.curve(n= '{component}_crv'.format(component= self.constructorName), p= curvePointList, d= 1)
            
            mc.setAttr('{node}.template'.format(node= connectionCurve), True)
            mc.setAttr('{node}.hiddenInOutliner'.format(node= connectionCurve), True)    
            
            
            self.nodesWithoutMasterList.append(connectionCurve)
            self.allList.append(connectionCurve)                 


            # create proxyChain classes
            
            for index, inputObjectValue in enumerate(self.inputObjects):
                self.outputDict[inputObjectValue]= proxyObject(componentName= self.limb, inputName= inputObjectValue, chosenColorValue= self.chosenColorValue, iterationInput= index, listLength= self.inputObjectsAmount)

                self.outputDict[inputObjectValue].createProxyObject()

                self.signifierList.append(self.outputDict[inputObjectValue].proxyObject)
                self.nodesWithoutMasterList.extend(self.outputDict[inputObjectValue].proxyComponentList)
                self.allList.extend(self.outputDict[inputObjectValue].proxyComponentList)

                # connect curvepoints to ProxyPositions
                mc.connectAttr('{matrixConnection}'.format(matrixConnection= self.outputDict[inputObjectValue].proxyWorldMtx), '{node}.controlPoints[{denumeration}]'.format(node= connectionCurve, denumeration= index))


            # finish off
            for item in self.nodesWithoutMasterList:
                mc.addAttr(item, ln='componentMembership', attributeType= 'message')
                mc.connectAttr('{node}.componentMaster'.format(node= self.constructor), '{node}.componentMembership'.format(node= item))

            for index, item in enumerate(self.signifierList):
                mc.addAttr(item, ln='proxyJoint', attributeType= 'message')
                mc.connectAttr('{node}.jointConnections'.format(node= self.constructor), '{node}.proxyJoint'.format(node= item))                    
                
                mc.addAttr(item, ln='order', attributeType= 'long')
                mc.setAttr('{node}.order'.format(node= item), index, lock= True)
                
            mc.parent(self.signifierList, self.constructor)

            return self.constructor
        
        else:
            print ('there were no input Objects specified for this instance')

    # CHAIN ALTERATIONS     ########################################################################################################################
    def renameProxyChain(self):
        print('not yet implemented')
    
    def recolorProxyChain(self):
        print('not yet implemented')
    
    def rebuildProxyChain(self):
        print('not yet implemented')
    


    # check for inputNode String, if nothing input, delete all proxy limbs
    def deleteProxyChain(self):
        # check if there is a valid constructor
        if mc.objExists(self.constructor):
            if mc.attributeQuery('componentMaster', node= self.constructor, exists= True):
                if self.allList:
                    mc.delete(self.allList)
                else:
                    print('there are no items in the allList')
            else:
                print('there is no valid attribute in {node}'.format(node= self.constructor))       
        
        else:
            print('there exists no {node}'.format(node= self.constructor))



    # create an aimed matrix, return the matrix while keeping the proxys in worldspace
    def getProxyInformation(self):

        # check if there is a valid constructor
        if mc.objExists(self.constructor):
            if mc.attributeQuery('componentMaster', node= self.constructor, exists= True):
                
                if self.outputDict:
                    dictLength= len(self.outputDict)
                    
                    if dictLength == 1:
                        for key in self.outputDict:
                            worldMatrix= mc.xform(self.outputDict[key].proxyObject, query= True, matrix= True, worldSpace= True)
                            self.zippedDict[key]= worldMatrix
                    
                    elif dictLength > 1:
                        # convert dictionary to a list for indexing
                        keyList=list(self.outputDict.keys())
                        
                        for index, value in enumerate(keyList):
                            
                            # declaring slave node
                            slaveNode= self.outputDict[keyList[index]].proxyObject

                            # get old world matrix to reset the node after calculation operation
                            oldWorldMatrix= mc.xform(slaveNode, query= True, matrix= True, worldSpace= True)
                            
                            # sort the list based on list index and create aim
                            # last object of the list
                            if index == (dictLength-1):
                                firstNodeIndex= -2
                                secondNodeIndex= -1
                                thirdNodeIndex= -1

                                aimVecDirection= -1

                            # middle objects of the list
                            elif index < (dictLength-1) and not index == 0:
                                firstNodeIndex= 1
                                secondNodeIndex= -1
                                thirdNodeIndex= 1

                                aimVecDirection= 1

                            # first object of the list
                            elif index == 0:
                                if dictLength > 2:
                                    firstNodeIndex= 1
                                    secondNodeIndex= 2
                                    thirdNodeIndex= 1

                                    aimVecDirection= 1

                                else:
                                    firstNodeIndex= 1
                                    secondNodeIndex= 1
                                    thirdNodeIndex= 1

                                    aimVecDirection= 1

                            firstNode= self.outputDict[keyList[index+firstNodeIndex]].proxyObject
                            secondNode= self.outputDict[keyList[index+secondNodeIndex]].proxyObject


                            # calculate the upVector ##############################################
                            upVectorResult= calculateCrossproduct(slaveNode, firstNode, secondNode)
                            

                            # look in which direction the upvector points and flip it if able
                            omUpVectorResult= om2.MVector(upVectorResult[0], upVectorResult[1], upVectorResult[2])
                            

                            if index == 0:
                                oldUpVec=  om2.MVector(0, 0, -1)


                            flippedVector= oldUpVec * omUpVectorResult


                            if flippedVector < 0:
                                omUpVectorResult= omUpVectorResult* -1

                            
                            # calculate the aimVector ##############################################
                            if index == 0:
                                omAimVector= om2.MVector(aimVecDirection,0,0)

                                omWorldMatrix= om2.MMatrix([oldWorldMatrix[0], oldWorldMatrix[1], oldWorldMatrix[2], oldWorldMatrix[3], 
                                                        oldWorldMatrix[4], oldWorldMatrix[5], oldWorldMatrix[6], oldWorldMatrix[7], 
                                                        oldWorldMatrix[8], oldWorldMatrix[9], oldWorldMatrix[10], oldWorldMatrix[11], 
                                                        oldWorldMatrix[12], oldWorldMatrix[13], oldWorldMatrix[14], oldWorldMatrix[15]])
                                
                                omWorldPositionMatrix= om2.MTransformationMatrix(omWorldMatrix)
                                omWorldTransformVector= omWorldPositionMatrix.translation(4) 	


                            # flip the vectors again based on the world X position
                            if omWorldTransformVector[0] < 0:
                                aimVecDirection *= -1
                                if index == 0:
                                    omUpVectorResult *= -1


                            upVectorResult= list(omUpVectorResult)
                            oldUpVec= omUpVectorResult


                            # apply the aimConstraint ##############################################
                            masterNode= self.outputDict[keyList[index+thirdNodeIndex]].proxyObject
                            aimConstraint= mc.aimConstraint(masterNode, slaveNode, worldUpType= 'vector', aimVector= [aimVecDirection,0,0], worldUpVector= upVectorResult)


                            mc.delete(aimConstraint)


                            # query the new matrix
                            worldMatrix= mc.xform(slaveNode, query= True, matrix= True, worldSpace= True)
                            
                            # reset to old world matrix
                            # mc.xform(slaveNode, matrix= oldWorldMatrix, worldSpace= True)
                        
                            self.zippedDict[slaveNode]= worldMatrix
                    
                    return self.zippedDict
                            
                else:
                    print('there are no items in "{list}"'.format(list= str(self.outputDict)))
                    return None
            else:
                print('there is no valid attribute in "{node}"'.format(node= self.constructor))       
                return None

        else:
            print('there exists no "{node}" node'.format(node= self.constructor))
            return None
